﻿using AutoMapper;
using Test.Data.DTO;
using Test.Data.Model;

namespace Test.ProfileMapper
{
    public class PostProfileMapper: Profile
    {
        public PostProfileMapper()
        {
            CreateMap<Post, PostDTO>();
        }
    }
}
