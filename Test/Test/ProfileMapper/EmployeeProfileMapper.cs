﻿using AutoMapper;
using Test.Data.DTO;
using Test.Data.Model;

namespace Test.ProfileMapper
{
    public class EmployeeProfileMapper: Profile
    {
        public EmployeeProfileMapper()
        {
            CreateMap<Employee, EmployeeDTO>();
        }

    }
}
