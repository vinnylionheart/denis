﻿using AutoMapper;
using Test.Data.DTO;
using Test.Data.Model;

namespace Test.ProfileMapper
{
    public class RoleProfileMapper: Profile
    {
        public RoleProfileMapper()
        {
            CreateMap<Role, RoleDTO>();
        }
    }
}
