﻿using AutoMapper;
using Test.Data.DTO;
using Test.Data.Model;

namespace Test.ProfileMapper
{
    public class DivisionProfileMapper: Profile
    {
        public DivisionProfileMapper()
        {
            CreateMap<Division, DivisionDTO>();
        }
    }
}
