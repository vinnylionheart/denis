﻿using AutoMapper;
using Test.Data.DTO;
using Test.Data.Model;

namespace Test.ProfileMapper
{
    public class UserProfileMapper: Profile
    {
        public UserProfileMapper()
        {
            CreateMap<User, UserDTO>();
        }
    }
}
