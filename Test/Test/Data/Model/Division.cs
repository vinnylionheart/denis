﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Test.Data.Model
{

    public class Division
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int ParentOrgStructureId { get; set; }
        [AllowNull]
        public Division ParentOrgStructure { get; set; }
        public List<Employee> Employees { get; set; }
        public Division()
        {
            Employees = new List<Employee>();
        }
    }
}
