﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Test.Data.Model
{
    public class Post
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public List<Employee> Employees { get; set; }
        public Post()
        {
            Employees = new List<Employee>();
        }
    }
}
