﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Test.Data.Model
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime Date { get; set; }
        [Required]
        public int DivisionsId { get; set; }
        public Division Divisions { get; set; }
        [Required]
        public int PostId { get; set; }
        public Post Posts { get; set; }
    }
}
