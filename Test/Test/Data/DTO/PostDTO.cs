﻿
namespace Test.Data.DTO
{
    public class PostDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
