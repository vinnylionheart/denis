﻿
namespace Test.Data.DTO
{
    public class DivisionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ParentOrgStructure { get; set; }
    }
}
