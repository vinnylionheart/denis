﻿using System;

namespace Test.Data.DTO
{
    public class EmployeeDTO
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime Date { get; set; }
        public string Divisions { get; set; }
        public string Posts { get; set; }
    }
}
