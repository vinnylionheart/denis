﻿using System.Linq;
using Test.Data.Model;

namespace Test.Data.Interface
{
    public interface IEmployee
    {
        bool SaveChanges();
        IQueryable<Employee> GetAllEmployees();
        Employee GetEmployeeById(int id);
        void CreateEmployee(Employee employee);
        void UpdateEmployee(Employee employee);
        void DeleteEmployee(Employee employee);
        //void FilterEmployeeList(string name);
    }
}
