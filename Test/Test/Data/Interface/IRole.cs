﻿using System.Collections.Generic;
using Test.Data.Model;

namespace Test.Data.Interface
{
    public interface IRole
    {
        bool SaveChanges();
        IEnumerable<Role> GetAllRoles();
        Role GetRoleById(int id);
        void CreateRole(Role role);
        void UpdateRole(Role role);
        void DeleteRole(Role role);
    }
}
