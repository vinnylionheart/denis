﻿using System.Collections.Generic;
using Test.Data.Model;

namespace Test.Data.Interface
{
    public interface IDivision
    {
        bool SaveChanges();
        IEnumerable<Division> GetAllDivisions();
        Division GetDivisionById(int id);
        void CreateDivision(Division division);
        void UpdateDivision(Division division);
        void DeleteDivision(Division division);
    }
}
