﻿using System.Collections.Generic;
using Test.Data.Model;

namespace Test.Data.Interface
{
    public interface IPost
    {
        bool SaveChanges();
        IEnumerable<Post> GetAllPosts();
        Post GetPostById(int id);
        void CreatePost(Post post);
        void UpdatePost(Post post);
        void DeletePost(Post post);
    }
}
