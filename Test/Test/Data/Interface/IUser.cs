﻿using System.Collections.Generic;
using Test.Data.Model;

namespace Test.Data.Interface
{
    public interface IUser
    {
        bool SaveChanges();
        IEnumerable<User> GetAllUsers();
        User GetUserById(int id);
        void CreateUser(User user);
        void UpdateUser(User user);
        void DeleteUser(User user);
    }
}
