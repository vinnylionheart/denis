﻿using System;
using System.Collections.Generic;
using System.Linq;
using Test.Data.Interface;
using Test.Data.Model;

namespace Test.Data.Repository
{
    public class PostService: IPost
    {
        private readonly AppDBContext _context;

        public PostService(AppDBContext context)
        {
            _context = context;
        }
        public void CreatePost(Post post)
        {
            if (post == null)
            {
                throw new ArgumentNullException(nameof(post));
            }

            _context.Post.Add(post);
        }

        public void DeletePost(Post post)
        {
            if (post == null)
            {
                throw new ArgumentNullException(nameof(post));
            }

            _context.Post.Remove(post);
        }

        public IEnumerable<Post> GetAllPosts()
        {
            return _context.Post.ToList();
        }

        public Post GetPostById(int id)
        {
            return _context.Post.FirstOrDefault(p => p.Id == id);
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() >= 0;
        }

        public void UpdatePost(Post post)
        {
            _context.Post.UpdateRange(post);
        }
    }
}
