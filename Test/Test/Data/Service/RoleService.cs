﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using Test.Data.DTO;
using Test.Data.Interface;
using Test.Data.Model;

namespace Test.Data.Repository
{
    public class RoleService : IRole
    {
        private readonly AppDBContext _context;

        public RoleService(AppDBContext context)
        {
            _context = context;
        }
        public void CreateRole(Role role)
        {
            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }

            _context.Role.Add(role);
        }

        public void DeleteRole(Role role)
        {
            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }

            _context.Role.Remove(role);
        }

        public IEnumerable<Role> GetAllRoles()
        {
            return _context.Role.ToList();
        }

        public Role GetRoleById(int id)
        {
            return _context.Role.FirstOrDefault(p => p.Id == id);
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() >= 0;
        }

        public void UpdateRole(Role role)
        {
            _context.Role.UpdateRange(role);
        }
    }
}
