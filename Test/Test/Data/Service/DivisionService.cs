﻿using System;
using System.Collections.Generic;
using System.Linq;
using Test.Data.Interface;
using Test.Data.Model;

namespace Test.Data.Repository
{
    public class DivisionService: IDivision
    {
        private readonly AppDBContext _context;

        public DivisionService(AppDBContext context)
        {
            _context = context;
        }
        public void CreateDivision(Division division)
        {
            if (division == null)
            {
                throw new ArgumentNullException(nameof(division));
            }

            _context.Division.Add(division);
        }

        public void DeleteDivision(Division division)
        {
            if (division == null)
            {
                throw new ArgumentNullException(nameof(division));
            }

            _context.Division.Remove(division);
        }

        public IEnumerable<Division> GetAllDivisions()
        {
            return _context.Division.ToList();
        }

        public Division GetDivisionById(int id)
        {
            return _context.Division.FirstOrDefault(p => p.Id == id);
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() >= 0;
        }

        public void UpdateDivision(Division division)
        {
            _context.Division.UpdateRange(division);
        }
    }
}
