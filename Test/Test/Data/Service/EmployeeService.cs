﻿using System;
using System.Collections.Generic;
using System.Linq;
using Test.Data.Interface;
using Test.Data.Model;

namespace Test.Data.Repository
{
    public class EmployeeService: IEmployee
    {
        private readonly AppDBContext _context;

        public EmployeeService(AppDBContext context)
        {
            _context = context;
        }
        public void CreateEmployee(Employee employee)
        {
            if (employee == null)
            {
                throw new ArgumentNullException(nameof(employee));
            }

            _context.Employee.Add(employee);
        }

        public void DeleteEmployee(Employee employee)
        {
            if (employee == null)
            {
                throw new ArgumentNullException(nameof(employee));
            }

            _context.Employee.Remove(employee);
        }

        public IQueryable<Employee> GetAllEmployees()
        {
            return _context.Employee;
        }

        public Employee GetEmployeeById(int id)
        {
            return _context.Employee.FirstOrDefault(p => p.Id == id);
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() >= 0;
        }

        public void UpdateEmployee(Employee employee)
        {
            _context.Employee.UpdateRange(employee);
        }

    }
}
