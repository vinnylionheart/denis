﻿using Microsoft.EntityFrameworkCore;
using Test.Data.Model;

namespace Test
{
    public class AppDBContext: DbContext
    {
        public AppDBContext()
        {
        }

        public AppDBContext(DbContextOptions<AppDBContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<Role> Role { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<Division> Division { get; set; }
        public DbSet<Post> Post { get; set; }
    }
}