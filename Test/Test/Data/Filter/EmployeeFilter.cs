﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test.Data.Model;

namespace Test.Data.Filter
{
    public static class EmployeeFilter
    {
        public static IQueryable<Employee> FilterEmployeeList(this IQueryable<Employee> employees, string name)
        {
            return employees.Where(x => x.Name.Contains(name) || x.LastName.Contains(name) || x.Surname.Contains(name) || x.Divisions.Name.Contains(name));
        }
    }
}
