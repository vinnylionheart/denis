﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Test.Data.DTO;
using Test.Data.Interface;
using Test.Data.Model;

namespace Test.Data.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private readonly IRole _role;
        private readonly IMapper _mapper;

        public RolesController(IRole role, IMapper mapper)
        {
            _role = role;
            _mapper = mapper;
        }

        // GET: api/Roles
        [HttpGet]
        public ActionResult <IEnumerable<RoleDTO>> GetRole()
        {
            var role = _role.GetAllRoles();

            return Ok(_mapper.Map<IEnumerable<RoleDTO>>(role));
        }

        // GET: api/Roles/5
        [HttpGet("{id}")]
        public ActionResult <RoleDTO> GetRole(int id)
        {
            var role = _role.GetRoleById(id);

            if (role == null) return NotFound();

            return Ok(_mapper.Map<RoleDTO>(role));

            
        }

        // PUT: api/Roles/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public ActionResult UpdateUser(int id, RoleDTO role)
        {
            var roleUpdate = _role.GetRoleById(id);
            if (roleUpdate == null) return NotFound();

            _mapper.Map(role, roleUpdate);

            _role.UpdateRole(roleUpdate);

            _role.SaveChanges();

            return NoContent();
        }

        // POST: api/Roles
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public ActionResult<RoleDTO> CreateRole(RoleDTO newRole)
        {
            var role = _mapper.Map<Role>(newRole);
            _role.CreateRole(role);
            _role.SaveChanges();

            var roleDTO = _mapper.Map<RoleDTO>(role);

            return CreatedAtRoute(nameof(GetRole), new { id = roleDTO.Id }, roleDTO);
        }

        // DELETE: api/Roles/5
        [HttpDelete("{id}")]
        public ActionResult DeleteRole(int id)
        {
            var roleDelete = _role.GetRoleById(id);
            if (roleDelete == null) return NotFound();
            _role.DeleteRole(roleDelete);
            _role.SaveChanges();

            return NoContent();
        }
    }
}
