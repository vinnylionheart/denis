﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Test.Data.DTO;
using Test.Data.Interface;
using Test.Data.Model;
using Test.Data.Filter;

namespace Test.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployee _employee;
        private readonly IMapper _mapper;

        public EmployeesController(IEmployee employee, IMapper mapper)
        {
            _employee = employee;
            _mapper = mapper;
        }

        // GET: api/Employees
        [HttpGet]
        public ActionResult<IEnumerable<EmployeeDTO>> GetEmployee()
        {
            var employee = _employee.GetAllEmployees();

            return Ok(_mapper.Map<IEnumerable<EmployeeDTO>>(employee));
        }

        // GET: api/Employees/5
        [HttpGet("{id}")]
        public ActionResult<EmployeeDTO> GetEmployee(int id)
        {
            var employee = _employee.GetEmployeeById(id);

            if (employee == null) return NotFound();

            return Ok(_mapper.Map<EmployeeDTO>(employee));
        }

        // PUT: api/Employees/5
        // To protect from overemployeeing attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public ActionResult UpdateEmployee(int id, EmployeeDTO employee)
        {
            var employeeUpdate = _employee.GetEmployeeById(id);
            if (employeeUpdate == null) return NotFound();

            _mapper.Map(employee, employeeUpdate);

            _employee.UpdateEmployee(employeeUpdate);

            _employee.SaveChanges();

            return NoContent();
        }

        // POST: api/Employees
        // To protect from overemployeeing attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public ActionResult<EmployeeDTO> CreateEmployee(EmployeeDTO newEmployee)
        {
            var employee = _mapper.Map<Employee>(newEmployee);
            _employee.CreateEmployee(employee);
            _employee.SaveChanges();

            var employeeDTO = _mapper.Map<EmployeeDTO>(employee);

            return CreatedAtRoute(nameof(GetEmployee), new { id = employeeDTO.Id }, employeeDTO);
        }

        // DELETE: api/Employees/5
        [HttpDelete("{id}")]
        public ActionResult DeleteEmployee(int id)
        {
            var employeeDelete = _employee.GetEmployeeById(id);
            if (employeeDelete == null) return NotFound();
            _employee.DeleteEmployee(employeeDelete);
            _employee.SaveChanges();

            return NoContent();
        }
        [HttpGet("/api/[controller]/ListWorker")]
        public ActionResult <IEnumerable<EmployeeDTO>> FilterEmployeeList(string name)
        {
            var employee = _employee.GetAllEmployees();

            if (name != null)
            {
                employee = employee.FilterEmployeeList(name);
            }
            return Ok(_mapper.Map<IEnumerable<EmployeeDTO>>(employee));
        }
    }
}
