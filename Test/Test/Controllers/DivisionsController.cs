﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Test.Data.DTO;
using Test.Data.Interface;
using Test.Data.Model;

namespace Test.Controllers
{   [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DivisionsController : ControllerBase
    {
        private readonly IDivision _division;
        private readonly IMapper _mapper;

        public DivisionsController(IDivision division, IMapper mapper)
        {
            _division = division;
            _mapper = mapper;
        }

        // GET: api/Divisions
        [HttpGet]
        public ActionResult<IEnumerable<DivisionDTO>> GetDivision()
        {
            var division = _division.GetAllDivisions();

            return Ok(_mapper.Map<IEnumerable<DivisionDTO>>(division));
        }

        // GET: api/Divisions/5
        [HttpGet("{id}")]
        public ActionResult<DivisionDTO> GetDivision(int id)
        {
            var division = _division.GetDivisionById(id);

            if (division == null) return NotFound();

            return Ok(_mapper.Map<DivisionDTO>(division));
        }

        // PUT: api/Divisions/5
        // To protect from overdivisioning attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public ActionResult UpdateDivision(int id, DivisionDTO division)
        {
            var divisionUpdate = _division.GetDivisionById(id);
            if (divisionUpdate == null) return NotFound();

            _mapper.Map(division, divisionUpdate);

            _division.UpdateDivision(divisionUpdate);

            _division.SaveChanges();

            return NoContent();
        }

        // POST: api/Divisions
        // To protect from overdivisioning attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public ActionResult<DivisionDTO> CreateDivision(DivisionDTO newDivision)
        {
            var division = _mapper.Map<Division>(newDivision);
            _division.CreateDivision(division);
            _division.SaveChanges();

            var divisionDTO = _mapper.Map<DivisionDTO>(division);

            return CreatedAtRoute(nameof(GetDivision), new { id = divisionDTO.Id }, divisionDTO);
        }

        // DELETE: api/Divisions/5
        [HttpDelete("{id}")]
        public ActionResult DeleteDivision(int id)
        {
            var divisionDelete = _division.GetDivisionById(id);
            if (divisionDelete == null) return NotFound();
            _division.DeleteDivision(divisionDelete);
            _division.SaveChanges();

            return NoContent();
        }
    }
}
