﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Test.Data.DTO;
using Test.Data.Interface;
using Test.Data.Model;

namespace Test.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly IPost _post;
        private readonly IMapper _mapper;

        public PostsController(IPost post, IMapper mapper)
        {
            _post = post;
            _mapper = mapper;
        }

        // GET: api/Posts
        [HttpGet]
        public ActionResult<IEnumerable<PostDTO>> GetPost()
        {
            var post = _post.GetAllPosts();

            return Ok(_mapper.Map<IEnumerable<PostDTO>>(post));
        }

        // GET: api/Posts/5
        [HttpGet("{id}")]
        public ActionResult<PostDTO> GetPost(int id)
        {
            var post = _post.GetPostById(id);

            if (post == null) return NotFound();

            return Ok(_mapper.Map<PostDTO>(post));
        }

        // PUT: api/Posts/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public ActionResult UpdatePost(int id, PostDTO post)
        {
            var postUpdate = _post.GetPostById(id);
            if (postUpdate == null) return NotFound();

            _mapper.Map(post, postUpdate);

            _post.UpdatePost(postUpdate);

            _post.SaveChanges();

            return NoContent();
        }

        // POST: api/Posts
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public ActionResult<PostDTO> CreatePost(PostDTO newPost)
        {
            var post = _mapper.Map<Post>(newPost);
            _post.CreatePost(post);
            _post.SaveChanges();

            var postDTO = _mapper.Map<PostDTO>(post);

            return CreatedAtRoute(nameof(GetPost), new { id = postDTO.Id }, postDTO);
        }

        // DELETE: api/Posts/5
        [HttpDelete("{id}")]
        public ActionResult DeletePost(int id)
        {
            var postDelete = _post.GetPostById(id);
            if (postDelete == null) return NotFound();
            _post.DeletePost(postDelete);
            _post.SaveChanges();

            return NoContent();
        }
    }
}
