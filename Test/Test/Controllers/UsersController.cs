﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Test.Data.DTO;
using Test.Data.Interface;
using Test.Data.Model;

namespace Test.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IRole _user;
        private readonly IMapper _mapper;

        public UsersController(IRole user, IMapper mapper)
        {
            _user = user;
            _mapper = mapper;
        }

        // GET: api/Users
        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> GetUser()
        {
            var user = _user.GetAllRoles();

            return Ok(_mapper.Map<IEnumerable<RoleDTO>>(user));
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public ActionResult <UserDTO> GetUser(int id)
        {
            var user = _user.GetRoleById(id);

            if (user == null) return NotFound();

            return Ok(_mapper.Map<RoleDTO>(user));
        }

        // PUT: api/Users/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public ActionResult UpdateRole(int id, UserDTO user)
        {
            var userUpdate = _user.GetRoleById(id);
            if (userUpdate == null) return NotFound();

            _mapper.Map(user, userUpdate);

            _user.UpdateRole(userUpdate);

            _user.SaveChanges();

            return NoContent();
        }

        // POST: api/Users
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public ActionResult<UserDTO> CreateUser(UserDTO newUser)
        {
            var user = _mapper.Map<Role>(newUser);
            _user.CreateRole(user);
            _user.SaveChanges();

            var userDTO = _mapper.Map<RoleDTO>(user);

            return CreatedAtRoute(nameof(GetUser), new { id = userDTO.Id }, userDTO);
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public ActionResult DeleteUser(int id)
        {
            var userDelete = _user.GetRoleById(id);
            if (userDelete == null) return NotFound();
            _user.DeleteRole(userDelete);
            _user.SaveChanges();

            return NoContent();
        }
    }
}
